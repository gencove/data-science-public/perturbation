import itertools
import cyvcf2
import random
import copy
import click
import math


def draw_bernoulli(p: float) -> bool:
    """
    Return `True` on success of bernoulli trial
    :param p: prob of success
    :return: bool
    """
    assert 0.0 <= p <= 1
    if random.random() < p:
        return True
    else:
        return False


class PerturbVCF:
    """
    perturbations
    """
    def __init__(self, ovcf_path: str, nvcf_path: str):
        """

        :param ovcf_path: input vcf path
        :param nvcf_path: output vcf path
        """
        self.ovcf_path = ovcf_path
        self.nvcf_path = nvcf_path

        # cyvcf2 VCF objects
        self.ovcf = cyvcf2.VCF(ovcf_path)
        self.nvcf = cyvcf2.Writer(self.nvcf_path, self.ovcf)

        self.allele_map = {0: 1, 1: 0}

    def perturb_phase(self, p: float) -> None:
        """
        Iterates through original vcf, perturbs phase based on bernoulli draw,
        i.e. flips phase at hets on success,
        and writes out new, perturbed VCF
        :param p: bernoulli dist parameter for success
        :return:
        """
        print(f"Perturbing phase at heterozygotes with probability {p}")
        # total hets and flips
        total_hets = 0
        total_flips = 0

        # variant counter
        variant_counter = 0

        # loop through all variants, then all heterozygous sites within those variants
        for variant in self.ovcf:
            variant_counter += 1
            if(variant_counter % 10000) == 0:
                print(f":: Processed {variant_counter} variants")
            original_genotypes = variant.genotypes

            # to store the perturbed genotypes at this variant
            new_genotypes = copy.deepcopy(original_genotypes)

            # indices at which a heterozygous site is present in the original vcf
            het_indices = []

            # counter for total flips at a given variant
            num_flips = 0

            # loop over every sample's genotypes
            for index, original_genotype in enumerate(original_genotypes):
                # only operate on het
                # original_genotype is a list like [0, 1, True]
                if sum(original_genotype[0:2]) == 1:
                    het_indices.append(index)
                    flip = draw_bernoulli(p)
                    if flip:
                        # simply rearrange the order (flip the phase) of the genotypes and insert
                        # into new genotypes list
                        new_genotype = [original_genotype[1], original_genotype[0], True]
                        new_genotypes[index] = new_genotype

                        # print(f"Flipping {original_genotype} to {new_genotype}")

                        num_flips += 1

            # print(f"flipped {num_flips} hets out of {len(het_indices)}")
            # print(f"original hets: {[original_genotypes[i] for i in het_indices]}")
            # print(f"new hets: {[new_genotypes[i] for i in het_indices]}")

            # we have to reassign the genotypes field in the original field according to docs
            # BUT don't write it to the original vcf, write to the new one
            variant.genotypes = new_genotypes

            # write out line to new vcf
            self.nvcf.write_record(variant)

            # increment counters
            total_hets += len(het_indices)
            total_flips += num_flips

        print(f"Total hets in VCF: {total_hets}")
        print(f"Total hets that had phased FLIPPED in VCF: {total_flips}, which is a proportion of {total_flips / total_hets}")
        self.nvcf.close()
        self.ovcf.close()

    def thin_variants(self, p: float) -> None:
        """
        Prune `p` proportion of variants from the original vcf
        :param p: proportion of variants to prune
        :return:
        """
        assert 0 < p < 1

        nvariants = 0

        # this is slightly hacky, but since the original VCF object is an iterator
        # we can't go over it twice, so fork it. but DON'T TOUCH THE ORIGINAL ITERATOR AFTER THIS
        ovcf1, ovcf2 = itertools.tee(self.ovcf, 2)

        # count records in order to figure out how many to discard
        for _ in ovcf1:
            nvariants += 1

        # number to retain
        nretain = math.ceil((1 - p) * nvariants)
        print(f"Total of {nvariants} variants in the vcf, so retaining {nretain} and discarding {nvariants - nretain}")

        # indices to retain
        indices_retain = random.sample(range(0, nvariants), k=nretain)


        variant_index = 0
        for variant in ovcf2:
            if variant_index in indices_retain:
                self.nvcf.write_record(variant)
            variant_index += 1
            if (variant_index % 10000) == 0:
                print(f":: Processed {variant_index} variants")

        self.nvcf.close()
        self.ovcf.close()

    def perturb_genotypes(self, p: float) -> None:
        """
        Iterates through original vcf, perturbs genotype based on bernoulli draw,
        i.e. flips 0->1 or 1->0 on success
        and writes out new, perturbed VCF
        :param p: bernoulli dist parameter for success
        :return:
        """
        # total variants and flips
        total_errors = 0

        # variant counter
        variant_counter = 0

        # allele counter
        total_alleles = 0

        i = 0

        # loop through all variants, then all samples
        for variant in self.ovcf:
            # i += 1
            # if i>10:
            #     break

            variant_counter += 1
            if (variant_counter % 10000) == 0:
                print(f":: Processed {variant_counter} variants")

            original_genotypes = variant.genotypes

            # to store the perturbed genotypes at this variant
            new_genotypes = copy.deepcopy(original_genotypes)

            # counter for total errors introduced at a given variant
            errors = 0

            # loop over every sample's genotypes
            for index, original_genotype in enumerate(original_genotypes):
                # original_genotype is a list like [0, 1, True]
                new_genotype = original_genotype

                # have to draw at the _allele_ level, not genotype level,
                # so another level of iteration
                for alindex in [0, 1]:
                    error = draw_bernoulli(p)

                    if error:
                        errors += 1
                        new_genotype[alindex] = self.allele_map[original_genotype[alindex]]

                # this is inefficient but whatever
                new_genotypes[index] = new_genotype

            # we have to reassign the genotypes field in the original field according to docs
            # BUT don't write it to the original vcf, write to the new one
            variant.genotypes = new_genotypes

            # write out line to new vcf
            self.nvcf.write_record(variant)

            # increment counters
            total_errors += errors
            total_alleles += 2 * len(original_genotypes)

        print(f"Total number of alleles flipped 0->1/1->0 in VCF: {total_errors}, which is a proportion of {total_errors / total_alleles} of all alleles")
        print(f"Total variants in VCF: {variant_counter}")
        print(f"Total alleles in VCF: {total_alleles}")
        self.nvcf.close()
        self.ovcf.close()


# -----------------------------------------------------------------------
# cli
@click.group()
def cli():
    pass


@cli.command()
@click.option("--pruneprop", default=0.5, required=True, help="The proportion of variants to randomly prune")
@click.option("--outvcf", required=True, help="Output path for pruned vcf")
@click.argument("vcf")
def prune(vcf, outvcf, pruneprop):
    pert = PerturbVCF(vcf, outvcf)
    pert.thin_variants(pruneprop)


@cli.command()
@click.option("--probsuccess", default = 0.5, required=True, help="The probability of success for a draw from a Bernoulli, whereupon success entails a het phase flip")
@click.option("--outvcf", required=True, help="Output path for phase-perturbed vcf")
@click.argument("vcf")
def phase(vcf, outvcf, probsuccess):
    pert = PerturbVCF(vcf, outvcf)
    pert.perturb_phase(probsuccess)


@cli.command()
@click.option("--probsuccess", default = 0.5, required=True, help="The probability of success for a draw from a Bernoulli, whereupon success entails an allele flip 0->1 or 1->0")
@click.option("--outvcf", required=True, help="Output path for genotype-perturbed vcf")
@click.argument("vcf")
def genotype(vcf, outvcf, probsuccess):
    pert = PerturbVCF(vcf, outvcf)
    pert.perturb_genotypes(probsuccess)


if __name__ == "__main__":
    cli()