# perturbation

This repository contains code used in the following publication: 

*The effects of reference panel perturbations on the accuracy of genotype imputation
Jeremiah H. Li, Andrew Liu, C. Alex Buerkle, William Palmer, Gillian M. Belbin, Mohammad Ahangari, Matthew J.S. Gibson, Lex Flagel
bioRxiv 2023.08.10.552684; doi: https://doi.org/10.1101/2023.08.10.552684*

# Samples evaluated

The FASTQ data used for imputation were acquired from the following link: https://www.internationalgenome.org/data-portal/data-collection/30x-grch38

The samples under evaluation are listed in `data/samples_to_use.tsv`.

# Perturbation 

The script used for the various perturbation modes can be found in `scripts/perturb.py`. 